class Contact {
  String name;
  String contact;

  Contact({
    required this.contact,
    required this.name,
  });
}
